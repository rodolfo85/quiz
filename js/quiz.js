$(document).ready(function(){
	
	// arrays
	var gamePieces = ['rock', 'paper', 'scissor', 'dynamite'];
	var humanScore = [];
	var computerScore = [];

	// variables
	var selectedItem;
	var computerItem;
	var combo;
	var state;
	var titleValue = 'Pick something';
	
	var humanTotalScore = 0;
	var computerTotalScore = 0;
	var win = 1;
	var lose = 0;
	
	// print the "gamePieces" array 
	$.each(gamePieces, function(index, value){
		$('.wrap-item').append('<div id="' + value + '" class="game-item"><img src="img/' + value + '.png" alt="' + value + '" /></div>');
	});

	// print the title value
	$('h1').text(titleValue);
	
	// get the random value from the array length
	function getRandomGamePiece(){
		var randomItem = Math.floor((Math.random() * gamePieces.length));
		computerItem = gamePieces[randomItem];
		return computerItem
	}

	// reset the match
	function reset(){
		$('.game-item').show().removeClass('block');
		$('.result, .wrap-input, .vs').remove();
		$('h1').text(titleValue);
		selectedItem = undefined;
		computerItem = undefined;
		combo = undefined;
		state = undefined;
	}

	// Score calculation and print the results
	function scoreCalculation(){
		// sum the score accumulated
		$.each(humanScore, function(index, value){
			humanTotalScore = humanTotalScore + value;
		});
		$.each(computerScore, function(index, value){
			computerTotalScore = computerTotalScore + value;
		});

		// print the arrays with the human score and the computer score
		$('.score').append('<h3>Final score</h3><p> You: ' + humanTotalScore + ' -  Computer: ' + computerTotalScore + '</p>');

		// who won ?
		if(humanTotalScore > computerTotalScore){
			$('.wrap-content').html('<h1>Compliments, You won!</h1>');
		}
		else if(humanTotalScore < computerTotalScore){
			$('.wrap-content').html('<h1>I\'m sorry, You lost!</h1>');
		}
		else{
			$('.wrap-content').html('<h1>it\'s pair!</h1>');
		}

		// button new game that refreshes the page
		$(".wrap-content").append('<div class="wrap-input"><input type="button" value="Start a new game" id="new-game"></div>');

		// refresh the page 
		$('#new-game').click(function(){
			location.reload(true);
		})

	}

	// show the selected item and make disappear the others 
	$('.game-item').click(function(){
		if(state == undefined){
			selectedItem = $(this).attr('id');
			$('.game-item').not(this).hide();
			// call the function to randomize the computer answer
			getRandomGamePiece();
			// print the computer answer
			$('.wrap-item').append('<span class="vs">VS</span><div id="' + computerItem + '" class="game-item result"><img src="img/' + computerItem + '.png" alt="' + computerItem + '" /></div>');
			$('.game-item').addClass('block');
			combo = selectedItem + "+" + computerItem;
			state = 1;

			// game logic to pick a winner
			switch(combo)
			{
				//rock
				case"rock+paper":
					$("h1").text("You lost");
					humanScore.push(lose);
					computerScore.push(win);
					break;

				case"rock+scissor":
					$("h1").text("You won");
					humanScore.push(win);
					computerScore.push(lose);
					break;

				case"rock+dynamite":
					$("h1").text("You lost");
					humanScore.push(lose);
					computerScore.push(win);
					break;

				case"rock+rock":
					$("h1").text("Pair");
					humanScore.push(win);
					computerScore.push(win);
					break;

				// paper
				case"paper+rock":
					$("h1").text("You won");
					humanScore.push(win);
					computerScore.push(lose);
					break;

				case"paper+dynamite":
					$("h1").text("You lost");
					humanScore.push(lose);
					computerScore.push(win);
					break;

				case"paper+scissor":
					$("h1").text("You lost");
					humanScore.push(lose);
					computerScore.push(win);
					break;

				case"paper+paper":
					$("h1").text("Pair");
					humanScore.push(win);
					computerScore.push(win);
					break;

				// scissor
				case"scissor+rock":
					$("h1").text("You lost");
					humanScore.push(lose);
					computerScore.push(win);
					break;

				case"scissor+dynamite":
					$("h1").text("You won");
					humanScore.push(win);
					computerScore.push(lose);
					break;

				case"scissor+paper":
					$("h1").text("You won");
					humanScore.push(win);
					computerScore.push(lose);
					break;

				case"scissor+scissor":
					$("h1").text("Pair");
					humanScore.push(win);
					computerScore.push(win);
					break;

				// dynamite
				case"dynamite+rock":
					$("h1").text("You won");
					humanScore.push(win);
					computerScore.push(lose);
					break;

				case"dynamite+scissor":
					$("h1").text("You lost");
					humanScore.push(lose);
					computerScore.push(win);
					break;

				case"dynamite+paper":
					$("h1").text("You won");
					humanScore.push(win);
					computerScore.push(lose);
					break;

				case"dynamite+dynamite":
					$("h1").text("Super explosion!");
					humanScore.push(win);
					computerScore.push(win);
					break;

			}
			// print the button to play again
			$(".wrap-content").append('<div class="wrap-input"><input type="button" value="Continue" id="continue"></div>');

			// print 'arrayScore'
			$('.score').append('<div><span>You:&nbsp;&nbsp;&nbsp;' + humanScore[humanScore.length - 1] + '</span> - <span>Computer:&nbsp;&nbsp;&nbsp;' + computerScore[computerScore.length - 1] + '</span></div>');

			// after 5 matches, the results are displayed and th game ends
			if(humanScore.length == 5 && computerScore.length == 5){
				scoreCalculation()
			}

		}
		else{
			// if the variable 'state' is set to '1', the click function on the game items is not going to do anything
			javascript:void(0);
		}

		// reset everything as the beginning
		$("#continue").click(function(){
			reset();
		})

	});


});
